FROM openjdk:8-jdk-alpine

WORKDIR /HelloWorld

COPY build/libs/hello-world-0.1.0.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","app.jar"]

